const crypto = require('crypto');
const rp = require('request-promise');
const uuid = require('uuid/v4');

const CONFIG = {
  url: `https://www.baokim.vn/the-cao/restFul/send`,
  merchant_id: '',
  api_username: '',
  api_password: '',
  secure_code: '',
  algo_mode: 'hmac',
  timeout: 55000
}

const setConfig = (options) => {
  CONFIG.merchant_id = options.merchant_id;
  CONFIG.api_username = options.api_username;
  CONFIG.api_password = options.api_password;
  CONFIG.secure_code = options.secure_code;
  options.algo_mode && (CONFIG.algo_mode = options.algo_mode);
  options.url && (CONFIG.url = options.url);
  options.timeout && (CONFIG.timeout = options.timeout);
}

const checkConfig = () => {
  let valid = false;
  if(CONFIG.merchant_id && CONFIG.api_username && CONFIG.api_password && CONFIG.secure_code) {
    valid = true;
  }

  return valid;
}

const makeSign = (pin, serial, type, transaction_id) => {
  const rawData = `${CONFIG.algo_mode}${CONFIG.api_password}${CONFIG.api_username}${type}${CONFIG.merchant_id}${pin}${serial}${transaction_id}`;
  return crypto.createHmac('sha1', CONFIG.secure_code).update(rawData).digest('hex');
}

const cardCharging = (pin, serial, type, cb) => {
  if(!checkConfig()) {
    throw new Error(`Not config for baokim-gateway yet!`);
  }

  const transaction_id = uuid();
  const data_sign = makeSign(pin, serial, type, transaction_id);

  makeRequest(pin, serial, type, transaction_id, data_sign, cb);
}

const makeRequest = (pin, serial, type, transaction_id, data_sign, cb) => {
  var options = {
    method: 'POST',
    uri: CONFIG.url,
    timeout: CONFIG.timeout,
    form: {
      algo_mode: CONFIG.algo_mode,
      api_password: CONFIG.api_password,
      api_username: CONFIG.api_username,
      card_id: type,
      merchant_id: CONFIG.merchant_id,
      pin_field: pin,
      seri_field: serial,
      transaction_id: transaction_id,
      data_sign: data_sign
    },
    json: true,
    simple: false,
    resolveWithFullResponse: true
  };

  rp(options)
    .then((res) => {
      cb(null, {
        transaction_id: transaction_id,
        status: res.statusCode,
        data: res.body
      })
    })
    .catch((err) => {
      cb(err);
    });
}

module.exports = {
  setConfig: setConfig,
  cardCharging: cardCharging
}
